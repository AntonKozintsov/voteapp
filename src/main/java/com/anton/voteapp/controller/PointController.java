package com.anton.voteapp.controller;

import com.anton.voteapp.entity.Point;
import com.anton.voteapp.entity.Topic;
import com.anton.voteapp.service.PointService;
import com.anton.voteapp.service.TopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/topics/points")
public class PointController {

    private final TopicService topicService;

    private final PointService pointService;

    @Autowired
    public PointController(TopicService topicService, PointService pointService) {
        this.topicService = topicService;
        this.pointService = pointService;
    }

    @PostMapping("/{topicId}")
    public Topic addPoint(@RequestBody Point point, @PathVariable Long topicId) {
        return topicService.save(topicId, point);
    }

    @GetMapping("/{pointId}")
    public Integer getPointStatistics(@PathVariable Long pointId) {
        return pointService.findById(pointId).getVotes();
    }

}
