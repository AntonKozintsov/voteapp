package com.anton.voteapp.controller;

import com.anton.voteapp.entity.Topic;
import com.anton.voteapp.service.TopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/topics")
public class TopicController {

    private final TopicService topicService;

    @Autowired
    public TopicController(TopicService topicService) {
        this.topicService = topicService;
    }

    @GetMapping
    public List<Topic> getTopics() {
        return topicService.findAll();
    }

    @GetMapping("/{topicUrl}")
    public Topic getTopic(@PathVariable String topicUrl) {
        return topicService.findByUrl(topicUrl);
    }

    @PutMapping("/start/{id}")
    public Topic startVote(@PathVariable Long id) {
        return topicService.save(id, true);
    }

    @PutMapping("/close/{id}")
    public Topic closeVote(@PathVariable Long id) {
        return topicService.save(id, false);
    }

    @PostMapping()
    public Topic addTopic(@RequestBody Topic topic) {
        return topicService.addTopic(topic);
    }


}
