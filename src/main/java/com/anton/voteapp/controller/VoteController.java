package com.anton.voteapp.controller;

import com.anton.voteapp.entity.Point;
import com.anton.voteapp.service.PointService;
import com.anton.voteapp.service.TopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/topics")
public class VoteController {

    private final PointService pointService;

    @Autowired
    public VoteController(PointService pointService) {
        this.pointService = pointService;
    }

    @GetMapping("/{topicUrl}/points/{pointId}")
    public Point doVote(@PathVariable String topicUrl, @PathVariable Long pointId) {
        return pointService.vote(topicUrl, pointId);

    }
}
