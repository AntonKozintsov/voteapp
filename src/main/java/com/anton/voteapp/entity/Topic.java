package com.anton.voteapp.entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@RequiredArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Entity
public class Topic {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private Boolean isActive;

    private String voteUrl;

    @OneToMany( cascade = CascadeType.ALL, targetEntity = Point.class )
    private List<Point> point;

}
