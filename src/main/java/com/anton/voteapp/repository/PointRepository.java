package com.anton.voteapp.repository;

import com.anton.voteapp.entity.Point;
import org.springframework.data.repository.CrudRepository;

public interface PointRepository extends CrudRepository<Point, Long> {

    Point findPointById(Long id);
}
