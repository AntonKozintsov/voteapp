package com.anton.voteapp.repository;

import com.anton.voteapp.entity.Topic;
import org.springframework.data.repository.CrudRepository;

public interface TopicRepository extends CrudRepository<Topic, Long> {

    Topic findTopicById(Long id);

    Topic findTopicByVoteUrl(String voteUrl);
}
