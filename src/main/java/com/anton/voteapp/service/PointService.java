package com.anton.voteapp.service;

import com.anton.voteapp.entity.Point;

import java.util.List;

public interface PointService {

    List<Point> findAll();

    Point vote(String topicUrl, Long pointId);

    Point findById(Long id);




}
