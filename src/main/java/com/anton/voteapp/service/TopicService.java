package com.anton.voteapp.service;

import com.anton.voteapp.entity.Point;
import com.anton.voteapp.entity.Topic;

import java.util.List;

public interface TopicService {

    Topic findByUrl(String topicUrl);

    List<Topic> findAll();

    Topic addTopic(Topic topic);

    Topic save(Topic topic);

    Topic save(Long topicId, Point point);

    Topic save(Long topicId, Boolean isActive);

}
