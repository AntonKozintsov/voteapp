package com.anton.voteapp.service.impl;

import com.anton.voteapp.entity.Point;
import com.anton.voteapp.entity.Topic;
import com.anton.voteapp.repository.PointRepository;
import com.anton.voteapp.service.PointService;
import com.anton.voteapp.service.TopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PointServiceImpl implements PointService {

    private final PointRepository pointRepository;

    private final TopicService topicService;

    @Autowired
    public PointServiceImpl(PointRepository pointRepository, TopicService topicService) {
        this.pointRepository = pointRepository;
        this.topicService = topicService;
    }

    @Override
    public List<Point> findAll() {
        return (List<Point>) pointRepository.findAll();
    }

    @Override
    public Point vote(String topicUrl, Long pointId) {
        Topic topic = topicService.findByUrl(topicUrl);
        Point point = pointRepository.findPointById(pointId);
        Integer votes = point.getVotes();
        if (topic.getIsActive()) {
            votes += 1;
            point.setVotes(votes);
        }
        return pointRepository.save(point);
    }

    @Override
    public Point findById(Long id) {
        return pointRepository.findPointById(id);
    }
}
