package com.anton.voteapp.service.impl;

import com.anton.voteapp.entity.Point;
import com.anton.voteapp.entity.Topic;
import com.anton.voteapp.repository.TopicRepository;
import com.anton.voteapp.service.TopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TopicServiceImpl implements TopicService {

    private final TopicRepository topicRepository;

    @Autowired
    public TopicServiceImpl(TopicRepository topicRepository) {
        this.topicRepository = topicRepository;
    }

    @Override
    public Topic findByUrl(String topicUrl) {
        Topic topic = topicRepository.findTopicByVoteUrl(topicUrl);
        if (topic==null) {
            throw new IllegalArgumentException();
        }
        return topicRepository.findTopicByVoteUrl(topicUrl);
    }

    @Override
    public List<Topic> findAll() {
        return (List<Topic>) topicRepository.findAll();
    }

    @Override
    public Topic addTopic(Topic topic) {
        topic.setIsActive(false);
        return topicRepository.save(topic);
    }

    @Override
    public Topic save(Topic topic) {
        return topicRepository.save(topic);
    }

    @Override
    public Topic save(Long id, Point point) {
        Topic topic = topicRepository.findTopicById(id);
        point.setVotes(0);
        List<Point> pointList = topic.getPoint();
        pointList.add(point);
        topic.setPoint(pointList);
        return topicRepository.save(topic);
    }

    @Override
    public Topic save(Long id, Boolean isActive) {
        Topic topic = topicRepository.findTopicById(id);
        topic.setIsActive(isActive);
        if (topic.getVoteUrl()==null) {
            topic.setVoteUrl(topic.getName()+topic.getId());
        }
        return topicRepository.save(topic);
    }


}
